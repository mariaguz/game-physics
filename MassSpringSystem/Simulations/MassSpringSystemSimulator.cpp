#include "MassSpringSystemSimulator.h"
using namespace std;
MassSpringSystemSimulator::MassSpringSystemSimulator()
{
	numberOfMassPoints = 0;
	numberOfSprings = 0;
	m_fSphereSize = 0.05f;
	m_fMass = 10;
	m_fStiffness = 40;
	m_fDamping = 0.6f;//todo decide
	m_iIntegrator = EULER;//todo decide
	m_externalForce = Vec3(0, 0, 0);
	m_gravity = Vec3(0.0, -8.0, 0.0);
	printed = false;
	isInitialStep = true;
	isGravity = false;
	isCollisionDetection = false;
	switch (m_iTestCase)
	{
	case 0://DEMO1
		cout << "in case 0" << endl; 
		isGravity = false;
		isCollisionDetection = false;
		printed = false;
		addMassPoint(Vec3(0, 0, 0), Vec3(-1, 0, 0), false);
		addMassPoint(Vec3(0, 2, 0), Vec3(1, 0, 0), false);
		addSpring(0, 1, 1);
		setIntegrator(EULER);
		setDampingFactor(0);
		break;
	case 1://Demo2
		isGravity = false;
		addMassPoint(Vec3(0, 0, 0), Vec3(-1, 0, 0), false);
		addMassPoint(Vec3(0, 2, 0), Vec3(1, 0, 0), false);
		addSpring(0, 1, 1);
		setIntegrator(EULER);
		setDampingFactor(0);
		break;
	case 2://Demo3
		isGravity = false;
		addMassPoint(Vec3(0, 0, 0), Vec3(-1, 0, 0), false);
		addMassPoint(Vec3(0, 2, 0), Vec3(1, 0, 0), false);
		addSpring(0, 1, 1);
		setIntegrator(MIDPOINT);
		setDampingFactor(0);
		break;
	case 3://Demo4
		break;
	case 4://Demo5
		isGravity = false;
		addMassPoint(Vec3(0, 0, 0), Vec3(-1, 0, 0), false);
		addMassPoint(Vec3(0, 2, 0), Vec3(1, 0, 0), false);
		addSpring(0, 1, 1);
		setIntegrator(LEAPFROG);
		setDampingFactor(0);
		break;
	case 5:
		isCollisionDetection = false;
		break;
	default:
		break;
	}
}
void MassSpringSystemSimulator::addSpring(int masspoint1, int masspoint2, float initialLength)
{
	Spring *newSpring = new Spring(masspoint1, masspoint2, initialLength, m_fStiffness);
	springs.push_back(newSpring);
	numberOfSprings++;
}
int MassSpringSystemSimulator::addMassPoint(Vec3 position, Vec3 Velocity, bool isFixed)
{
	MassPoint *newPoint = new MassPoint(position, Velocity, m_fMass, isFixed);
	massPoints.push_back(newPoint);
	numberOfMassPoints++;
	return massPoints.size()-1;
}

void MassSpringSystemSimulator::addGravity()
{
	applyExternalForce(m_gravity);
}

void MassSpringSystemSimulator::euler_step(float timeStep)
{
	for (vector<Spring *>::iterator it = springs.begin(); it != springs.end(); ++it)
	{
		MassPoint *mp1 = massPoints[(*it)->massPoint1];
		MassPoint *mp2 = massPoints[(*it)->massPoint2];
		float length = sqrt(mp1->pos.squaredDistanceTo(mp2->pos));
		mp1->force += (-1)*(*it)->stiffness*(length-(*it)->initialLength)*(1/length)*(mp1->pos-mp2->pos);
		mp2->force += (-1)*(*it)->stiffness*(length-(*it)->initialLength)*(1/length)*(mp2->pos-mp1->pos);
		Vec3 acc1 = mp1->force / mp1->mass;
		Vec3 acc2 = mp2->force / mp2->mass;
		/**
		if (!printed)
		{
			cout << "during e:" << endl;
			printMassPoints();
			cout << "acc1:" << acc1 << "\nacc2:" << acc2 << endl;
			cout << "f1:" << mp1->force << "\nf2:" << mp2->force << "\nexternal:" << m_externalForce << endl;
			cout << "mp1 pos:" << mp1->pos << "\nmp2 pos:" << mp2->pos << endl;
			cout << "mp1 mass:" << mp1->mass << "\nmp2 mass:" << mp2->mass << endl;
			cout << "length:" << length << "\nstiffness:" << (*it)->stiffness << "\ntimestep:" << timeStep << endl;
			cout << "squared length:" << mp1->pos.squaredDistanceTo(mp2->pos) << "\nsqrt check:" << sqrt(4) << endl;
		}*/
		mp1->adjust_pos(mp1->velocity*timeStep + mp1->pos);
		mp2->adjust_pos(mp2->velocity*timeStep + mp2->pos);
		mp1->velocity = acc1*timeStep + mp1->velocity;
		mp2->velocity = acc2*timeStep + mp2->velocity;
	}
}
void MassSpringSystemSimulator::midpoint_step(float timeStep)
{
	for (vector<Spring *>::iterator it = springs.begin(); it != springs.end(); ++it)
	{
		MassPoint *mp1 = massPoints[(*it)->massPoint1];
		MassPoint *mp2 = massPoints[(*it)->massPoint2];
		float length = sqrt(mp1->pos.squaredDistanceTo(mp2->pos));
		mp1->force += (-1)*(*it)->stiffness*(length-(*it)->initialLength)*(1/length)*(mp1->pos-mp2->pos);
		mp2->force += (-1)*(*it)->stiffness*(length - (*it)->initialLength)*(1 / length)*(mp2->pos - mp1->pos);
		Vec3 acc1 = mp1->force / mp1->mass;
		Vec3 acc2 = mp2->force / mp2->mass;
		Vec3 midVelocity1 = (0.5*acc1*timeStep) + mp1->velocity;
		Vec3 midVelocity2 = (0.5*acc2*timeStep) + mp2->velocity;
		Vec3 midPos1 = (0.5*mp1->velocity*timeStep) + mp1->pos;
		Vec3 midPos2 = (0.5*mp2->velocity*timeStep) + mp2->pos;
		float midLength = sqrt(midPos1.squaredDistanceTo(midPos2));
		Vec3 midForce1 = (-1)*(*it)->stiffness*(midLength - (*it)->initialLength)*(1/midLength)*(midPos1-midPos2);
		midForce1 += (m_externalForce - m_fDamping*midVelocity1);
		Vec3 midForce2 = (-1)*(*it)->stiffness*(midLength - (*it)->initialLength)*(1/midLength)*(midPos2-midPos1);
		midForce2 += (m_externalForce - m_fDamping*midVelocity2);
		if (isGravity)
		{
			midForce1 += m_gravity;
			midForce2 += m_gravity;
		}
		Vec3 midAcc1 = midForce1/mp1->mass;
		Vec3 midAcc2 = midForce2/mp2->mass;

		mp1->adjust_pos(midVelocity1*timeStep + mp1->pos);
		mp2->adjust_pos(midVelocity2*timeStep + mp2->pos);
		mp1->velocity = midAcc1*timeStep + mp1->velocity;
		mp2->velocity = midAcc2*timeStep + mp2->velocity;
	}
}

void MassSpringSystemSimulator::leapfrog_step(float timeStep)
{
	for (vector<Spring *>::iterator it = springs.begin(); it != springs.end(); ++it)
	{
		MassPoint *mp1 = massPoints[(*it)->massPoint1];
		MassPoint *mp2 = massPoints[(*it)->massPoint2];
		float length = sqrt(mp1->pos.squaredDistanceTo(mp2->pos));
		mp1->force += (-1)*(*it)->stiffness*(length-(*it)->initialLength)*(1/length)*(mp1->pos-mp2->pos);
		mp2->force += (-1)*(*it)->stiffness*(length-(*it)->initialLength)*(1/length)*(mp2->pos-mp1->pos);
		Vec3 acc1 = mp1->force / mp1->mass;
		Vec3 acc2 = mp2->force / mp2->mass;

		if (isInitialStep)
		{
			mp1->velocity = (-0.5*acc1*timeStep) + mp1->velocity;
			mp2->velocity = (-0.5*acc2*timeStep) + mp2->velocity;
			isInitialStep = false;
		}

		mp1->velocity = (acc1*timeStep) + mp1->velocity;
		mp2->velocity = (acc2*timeStep) + mp2->velocity;
		
		mp1->adjust_pos(mp1->velocity*timeStep + mp1->pos);
		mp2->adjust_pos(mp2->velocity*timeStep + mp2->pos);
		

	}
}

void MassSpringSystemSimulator::drawSprings()
{
	for (vector<Spring *>::iterator it = springs.begin(); it != springs.end(); ++it)
	{
		DUC->beginLine();
		DUC->drawLine(massPoints[(*it)->massPoint1]->pos, Vec3(0.5, 0.5, 0.5), massPoints[(*it)->massPoint2]->pos, Vec3(0.5, 0.5, 0.5));
		DUC->endLine();
	}
}
void MassSpringSystemSimulator::setMass(float mass)
{
	m_fMass = mass;
}
void MassSpringSystemSimulator::setIntegrator(int integrator)
{
	cout << "switching integrator" << endl;
	m_iIntegrator = integrator;
}
void MassSpringSystemSimulator::setStiffness(float stiffness)
{
	m_fStiffness = stiffness;
}
void MassSpringSystemSimulator::setDampingFactor(float damping)
{
	m_fDamping = damping;
}
int MassSpringSystemSimulator::getNumberOfMassPoints()
{
	return massPoints.size();
}
int MassSpringSystemSimulator::getNumberOfSprings()
{
	return springs.size();
}
Vec3 MassSpringSystemSimulator::getPositionOfMassPoint(int index)
{
	return massPoints[index]->pos;
}
Vec3 MassSpringSystemSimulator::getVelocityOfMassPoint(int index)
{
	return massPoints[index]->velocity;
}

const char * MassSpringSystemSimulator::getTestCasesStr(){
	return "Demo1, Demo2, Demo3, Demo4, Demo5";
}

const char * MassSpringSystemSimulator::getIntegratorsCasesStr(){
	return "Euler, Leapfrog, Midpoint";
}

const char * MassSpringSystemSimulator::getGravityCasesStr(){
	return "Yes, No";
}

void MassSpringSystemSimulator::reset(){
		m_mouse.x = m_mouse.y = 0;
		m_trackmouse.x = m_trackmouse.y = 0;
		m_oldtrackmouse.x = m_oldtrackmouse.y = 0;
		massPoints.clear();
		springs.clear();
		numberOfMassPoints = 0;
		numberOfSprings = 0;
}
void MassSpringSystemSimulator::initUI(DrawingUtilitiesClass * DUC)
{
	this->DUC = DUC;
	TwType TW_TYPE_TESTCASE = TwDefineEnumFromString("Integrator", getIntegratorsCasesStr());
	switch (m_iTestCase)
	{
	case 0:break;
	case 1:
		break;
	case 2:
		break;
	case 3:
		TwAddVarRW(DUC->g_pTweakBar, "Damping", TW_TYPE_FLOAT, &m_fDamping, "step=0.05 min=0.01");
		TwAddVarRW(DUC->g_pTweakBar, "Integrator", TW_TYPE_TESTCASE, &m_iIntegrator, "");
		TwAddVarRW(DUC->g_pTweakBar, "Gravity", TW_TYPE_BOOLCPP, &isGravity, "");
		break;
	case 4:
		break;
	default:
		break;
	}
	TwAddVarRW(DUC->g_pTweakBar, "Collision detection", TW_TYPE_BOOLCPP, &isCollisionDetection, "");
}
void MassSpringSystemSimulator::notifyCaseChanged(int testCase)
{
	m_iTestCase = testCase;
	isInitialStep = true;
	switch (m_iTestCase)
	{
	case 0:
		cout << "demo1 !\n";
		reset();
		isCollisionDetection = false;
		isGravity = false;
		printed = false;
		addMassPoint(Vec3(0, 0, 0), Vec3(-1, 0, 0), false);
		addMassPoint(Vec3(0, 2, 0), Vec3(1, 0, 0), false);
		addSpring(0, 1, 1);
		setIntegrator(EULER);
		setDampingFactor(0);
		break;
	case 1:
		cout << "demo2!\n";
		reset();
		isGravity = false;
		addMassPoint(Vec3(0, 0, 0), Vec3(-1, 0, 0), false);
		addMassPoint(Vec3(0, 2, 0), Vec3(1, 0, 0), false);
		addSpring(0, 1, 1);
		setIntegrator(EULER);
		setDampingFactor(0);
		break;
	case 2:
		cout << "demo3 !\n";
		reset();
		isGravity = false;
		addMassPoint(Vec3(0, 0, 0), Vec3(-1, 0, 0), false);
		addMassPoint(Vec3(0, 2, 0), Vec3(1, 0, 0), false);
		addSpring(0, 1, 1);
		setIntegrator(MIDPOINT);
		setDampingFactor(0);
		break;
	case 3:
		cout << "demo4 !\n";
		reset();
		/**addMassPoint(Vec3(0, 0, 0), Vec3(0, 0, 0), false);
		addMassPoint(Vec3(0, 1, 1), Vec3(0, 0, 0), false);
		addMassPoint(Vec3(0, 1, 0), Vec3(0, 0, 0), false);
		addMassPoint(Vec3(0, 1, 1), Vec3(0, 0, 0), false);
		addMassPoint(Vec3(1, 0, 0), Vec3(0, 0, 0), false);
		addMassPoint(Vec3(1, 0, 1), Vec3(0, 0, 0), false);
		addMassPoint(Vec3(1, 1, 0), Vec3(0, 0, 0), false);
		addMassPoint(Vec3(1, 1, 1), Vec3(0, 0, 0), false);
		addMassPoint(Vec3(0, 2, 0), Vec3(0, 0, 0), false);
		addMassPoint(Vec3(0, -2, 0), Vec3(0, 0, 0), false);*/
		addMassPoint(Vec3(0, 0, 0), Vec3(-1, 0, 0), false);
		addMassPoint(Vec3(0, 1, 1), Vec3(1, 0, 0), false);
		addMassPoint(Vec3(0, 1, 0), Vec3(-1, 0, 0), false);
		addMassPoint(Vec3(0, 1, 1), Vec3(1, 0, 0), false);
		addMassPoint(Vec3(1, 0, 0), Vec3(-1, 0, 0), false);
		addMassPoint(Vec3(1, 0, 1), Vec3(1, 0, 0), false);
		addMassPoint(Vec3(1, 1, 0), Vec3(-1, 0, 0), false);
		addMassPoint(Vec3(1, 1, 1), Vec3(1, 0, 0), false);
		addMassPoint(Vec3(0, 2, 0), Vec3(-1, 0, 0), false);
		addMassPoint(Vec3(0, -2, 0), Vec3(1, 0, 0), false);
		addSpring(0, 1, 0.5);
		addSpring(0, 2, 0.5);
		addSpring(0, 3, 0.5);
		addSpring(0, 4, 1);
		addSpring(0, 5, 1);
		addSpring(0, 6, 1);
		addSpring(0, 7, 1);
		addSpring(0, 8, 1);
		addSpring(0, 9, 1);
		addSpring(1, 2, 1);
		addSpring(1, 3, 1);
		addSpring(1, 4, 1);
		addSpring(1, 5, 1);
		addSpring(1, 6, 1);
		addSpring(1, 7, 1);
		addSpring(1, 8, 1);
		addSpring(1, 9, 1);
		addSpring(2, 3, 1);
		addSpring(2, 4, 1);
		addSpring(2, 5, 1);
		addSpring(2, 6, 1);
		addSpring(2, 7, 1);
		addSpring(2, 8, 1);
		addSpring(2, 9, 1);
		addSpring(3, 4, 1);
		addSpring(3, 6, 1);
		addSpring(3, 5, 1);
		addSpring(3, 7, 1);
		addSpring(3, 8, 1);
		addSpring(3, 9, 1);
		addSpring(4, 5, 1);
		addSpring(4, 6, 1);
		addSpring(4, 7, 1);
		addSpring(4, 8, 1);
		addSpring(4, 9, 1);
		addSpring(5, 6, 1);
		addSpring(5, 7, 1);
		addSpring(5, 8, 1);
		addSpring(5, 9, 1);
		addSpring(6, 7, 1);
		addSpring(6, 8, 1);
		addSpring(6, 9, 1);
		addSpring(7, 8, 1);
		addSpring(7, 9, 1);
		addSpring(8, 9, 1);
		break;
	case 4:
		reset();
		isCollisionDetection = false;
		isGravity = false;
		addMassPoint(Vec3(0, 0, 0), Vec3(-1, 0, 0), false);
		addMassPoint(Vec3(0, 2, 0), Vec3(1, 0, 0), false);
		addSpring(0, 1, 1);
		setIntegrator(LEAPFROG);
		setDampingFactor(0);
		break;
	case 5:
		isCollisionDetection = false;
		break;
	default:
		cout << "Empty Test!\n";
		break;
	}
}
void MassSpringSystemSimulator::externalForcesCalculations(float timeElapsed)
{
	for (vector<MassPoint *>::iterator mpIt = massPoints.begin(); mpIt != massPoints.end(); ++mpIt)
	{
		(*mpIt)->force = m_externalForce - m_fDamping*(*mpIt)->velocity;
		if (isGravity)
		{
			(*mpIt)->force += m_gravity;
		}
	}
	
}

void MassSpringSystemSimulator::applyExternalForce(Vec3 force)
{
	m_externalForce += force;
}

void MassSpringSystemSimulator::performStep(float timeStep)
{
	switch(m_iIntegrator)
	{
	case EULER:
		euler_step(timeStep);
		break;
	case MIDPOINT:
		midpoint_step(timeStep);
		break;
	case LEAPFROG:
		leapfrog_step(timeStep);
		break;
	default:
		break;
	}
}

void MassSpringSystemSimulator::simulateTimestep(float timeStep)
{
	externalForcesCalculations(timeStep);
	// update current setup for each frame
	switch (m_iTestCase)
	{// handling different cases
	case 0:
		// Demo1
		if (!printed)
		{
			isCollisionDetection = false;
			performStep(TIMESTEP_DEMO1);
			cout << "results for euler step: " << endl;
			printMassPoints();
			reset();
			addMassPoint(Vec3(0, 0, 0), Vec3(-1, 0, 0), false);
			addMassPoint(Vec3(0, 2, 0), Vec3(1, 0, 0), false);
			addSpring(0, 1, 1);
			setIntegrator(MIDPOINT);
			performStep(TIMESTEP_DEMO1);
			cout << "results for midpoint step: " << endl;
			printMassPoints();
			
			reset();
			addMassPoint(Vec3(0, 0, 0), Vec3(-1, 0, 0), false);
			addMassPoint(Vec3(0, 2, 0), Vec3(1, 0, 0), false);
			addSpring(0, 1, 1);
			setIntegrator(LEAPFROG);
			performStep(TIMESTEP_DEMO1);
			cout << "results for leapfrog step: " << endl;
			printMassPoints();
			printed = true;
			cout << "damping:" << m_fDamping << endl;
		}
		break;
	case 1:
		// Demo2
		performStep(TIMESTEP_DEMO2);
		break;
	case 2:
		//Demo3
		performStep(TIMESTEP_DEMO3);
		break;
	case 3:
		//Demo4
		performStep(timeStep);
		break;
	case 4:
		//Demo4
		cout << "sim ts d5" << endl;
		performStep(TIMESTEP_DEMO5);
		break;
	case 5:
		//for tests
		performStep(timeStep);
		break;
	default:
		break;
	}
	if (isCollisionDetection)
	{
		correctCollision();
	}

}

void MassSpringSystemSimulator::correctCollision()
{
	float lim = 0.5f;
	for (vector<MassPoint *>::iterator it = massPoints.begin(); it != massPoints.end(); ++it)
	{
		if ((*it)->pos.x < -lim + m_fSphereSize) {
			(*it)->pos.x = -lim + m_fSphereSize;
		}
		if ((*it)->pos.x > lim - m_fSphereSize) {
			(*it)->pos.x = lim - m_fSphereSize;
		}
		if ((*it)->pos.y < -lim + m_fSphereSize) {
			(*it)->pos.y = -lim + m_fSphereSize;
		}
		if ((*it)->pos.y > lim - m_fSphereSize) {
			(*it)->pos.y = lim - m_fSphereSize;
		}
		if ((*it)->pos.z < -lim + m_fSphereSize) {
			(*it)->pos.z = -lim + m_fSphereSize;
		}
		if ((*it)->pos.z > lim - m_fSphereSize) {
			(*it)->pos.z = lim - m_fSphereSize;
		}
	}
}

void MassSpringSystemSimulator::drawDemo1()
{
	//cout << "d1:" << printed << endl;
	if (!printed)
	{
		cout << "position of p0:" << massPoints[0]->pos.x << "," << massPoints[0]->pos.y << "," << massPoints[0]->pos.z << endl;
		cout << "position of p1:" << massPoints[1]->pos << endl;
		cout << "velocity of p0:" << massPoints[0]->velocity << endl;
		cout << "velocity of p1:" << massPoints[1]->velocity << endl;
		printed = true;
	}
}
void MassSpringSystemSimulator::drawDemo2()
{
	std::mt19937 eng;
    std::uniform_real_distribution<float> randCol( 0.0f, 1.0f);
    std::uniform_real_distribution<float> randPos(-0.5f, 0.5f);
	for (int i=0; i<numberOfMassPoints; i++)
    {
		DUC->setUpLighting(Vec3(),0.4*Vec3(1,1,1),100,0.6*Vec3(randCol(eng),randCol(eng), randCol(eng)));
		DUC->drawSphere(massPoints[i]->pos,Vec3(m_fSphereSize, m_fSphereSize, m_fSphereSize));
    }
	drawSprings();
	//printMassPoints();
}

void MassSpringSystemSimulator::drawDemo3()
{
	std::mt19937 eng;
    std::uniform_real_distribution<float> randCol( 0.0f, 1.0f);
    std::uniform_real_distribution<float> randPos(-0.5f, 0.5f);
	for (int i=0; i<numberOfMassPoints; i++)
    {
		DUC->setUpLighting(Vec3(),0.4*Vec3(1,1,1),100,0.6*Vec3(randCol(eng),randCol(eng), randCol(eng)));
		DUC->drawSphere(massPoints[i]->pos,Vec3(m_fSphereSize, m_fSphereSize, m_fSphereSize));
    }
	drawSprings();
	//printMassPoints();
}


void MassSpringSystemSimulator::drawDemo4()
{
	std::mt19937 eng;
    std::uniform_real_distribution<float> randCol( 0.0f, 1.0f);
    std::uniform_real_distribution<float> randPos(-0.5f, 0.5f);
	for (int i=0; i<numberOfMassPoints; i++)
    {
		DUC->setUpLighting(Vec3(),0.4*Vec3(1,1,1),100,0.6*Vec3(randCol(eng),randCol(eng), randCol(eng)));
		DUC->drawSphere(massPoints[i]->pos,Vec3(m_fSphereSize, m_fSphereSize, m_fSphereSize));
    }
	drawSprings();
	//printMassPoints();
}

void MassSpringSystemSimulator::drawDemo5()
{
	std::mt19937 eng;
    std::uniform_real_distribution<float> randCol( 0.0f, 1.0f);
    std::uniform_real_distribution<float> randPos(-0.5f, 0.5f);
	for (int i=0; i<numberOfMassPoints; i++)
    {
		DUC->setUpLighting(Vec3(),0.4*Vec3(1,1,1),100,0.6*Vec3(randCol(eng),randCol(eng), randCol(eng)));
		DUC->drawSphere(massPoints[i]->pos,Vec3(m_fSphereSize, m_fSphereSize, m_fSphereSize));
    }
	drawSprings();
	//printMassPoints();
}

void MassSpringSystemSimulator::drawFrame(ID3D11DeviceContext* pd3dImmediateContext)
{
	switch(m_iTestCase)
	{
	case 0: drawDemo1();break;
	case 1: drawDemo2();break;
	case 2: drawDemo3();break;
	case 3: drawDemo4();break;
	case 4: drawDemo5();break;
	}
}
void MassSpringSystemSimulator::printMassPoints()
{
	for (int i=0; i < numberOfMassPoints; ++i)
	{
		cout << "position of mass point number " << i << " is:" << massPoints[i]->pos << endl; 
		cout << "velocity of mass point number " << i << " is:" << massPoints[i]->velocity << endl;
	}
}
void MassSpringSystemSimulator::onClick(int x, int y)
{
	m_trackmouse.x += x - m_oldtrackmouse.x;
	m_trackmouse.y += y - m_oldtrackmouse.y;
}
void MassSpringSystemSimulator::onMouse(int x, int y)
{
	m_oldtrackmouse.x = x;
	m_oldtrackmouse.y = y;
};

