#ifndef RIGIDBODYSYSTEMSIMULATOR_h
#define RIGIDBODYSYSTEMSIMULATOR_h
#include "Simulator.h"
#include "RigidBodySystem.h"
#include "collisionDetect.h"

#include <vector>

#define TESTCASEUSEDTORUNTEST 2
#define DEMO1_TIMESTEP 2.0f
#define DEMO2_TIMESTEP 0.01f

#define ELASTICITY 1.0f

class RigidBodySystemSimulator :public Simulator {
public:
	// Construtors
	RigidBodySystemSimulator();

	// Functions
	const char * getTestCasesStr();
	void initUI(DrawingUtilitiesClass * DUC);
	void reset();
	void drawFrame(ID3D11DeviceContext* pd3dImmediateContext);
	void notifyCaseChanged(int testCase);
	void externalForcesCalculations(float timeElapsed);
	void simulateTimestep(float timeStep);
	void onClick(int x, int y);
	void onMouse(int x, int y);

	// ExtraFunctions
	int getNumberOfRigidBodies();
	Vec3 getPositionOfRigidBody(int i);
	Vec3 getLinearVelocityOfRigidBody(int i);
	Vec3 getAngularVelocityOfRigidBody(int i);
	void applyForceOnBody(int i, Vec3 loc, Vec3 force);
	void addRigidBody(Vec3 position, Vec3 size, int mass);
	void setOrientationOf(int i, Quat orientation);
	void setVelocityOf(int i, Vec3 velocity);
	void resetInertiaTensor(int i);

	void drawDemo1();
	void drawDemo2();
	void drawDemo3();
	void drawDemo4();
	void drawBox(RigidBodySystem body);
	void drawAllBoxes();
	void single_print(string str);
	//const char * getCurrentBoxCasesStr2();
	const char * getCurrentBoxCasesStr();

	void perform_time_step(float timestep);
private:
	// Attributes
	std::vector<RigidBodySystem> rigidbodies;
	Vec3 m_externalForce;
	bool rotator;
	int currInd;
	bool printed;
	bool printed2;
	bool forcer;

	// UI Attributes
	Point2D m_mouse;
	Point2D m_trackmouse;
	Point2D m_oldtrackmouse;
};
#endif