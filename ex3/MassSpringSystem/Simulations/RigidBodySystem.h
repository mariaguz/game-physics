#ifndef RIGIDB_H
#define RIGIDB_H
#include "Simulator.h"
#include <DirectXMath.h>
typedef XMVECTOR QVec;
typedef XMMATRIX Matr4;

class RigidBodySystem
{
public:
	float mass;
	Vec3 position;
	Vec3 velocity;
	Vec3 size;
	Mat4 inertiaTensorInverse0;
	Mat4 inertiaTensorInverse;
	XMFLOAT4 orientation0;
	GamePhysics::Quat orientation;
	Vec3 angularMomentum;
	Vec3 angularVelocity;
	Vec3 forceAccumulator;
	Vec3 torqueAccumulator;
	Vec3 colVelocity;
	Vec3 colAngularMomentum;

	RigidBodySystem(Vec3 position, Vec3 size, float mass)
		: position(position), size(size), mass(mass)
	{
		// Initialize inertia tensor for symmetric rectangular prism
		// http://www-robotics.cs.umass.edu/~grupen/603/slides/DynamicsI.pdf

		//float w = size[0]; //x
		//float l = size[1]; //y
		//float h = size[2]; //z
		//float iW = mass*((h*h) + (l*l)) / 12;
		//float iL = mass*((w*w) + (h*h)) / 12;
		//float iH = mass*((l*l) + (w*w)) / 12;
		//Mat4 invI(1.0 / iW, 0.0f, 0.0f, 0.0f,
		//	0.0f, 1.0 / iL, 0.0f, 0.0f,
		//	0.0f, 0.0f, 1.0 / iH, 0.0f,
		//	0.0f, 0.0f, 0.0f, 0.0f);
		//inertiaTensorInverse = invI;
		//inertiaTensorInverse.inverse();
		//inertiaTensorInverse0 = inertiaTensorInverse;
		float x = size[0];
		float y = size[1];
		float z = size[2];
		float xS = x*x;
		float yS = y*y;
		float zS = z*z;
		inertiaTensorInverse0 = Mat4(12.0 / mass / (yS + zS), 0.0, 0.0, 0.0,
			0.0, 12.0 / mass / (xS + zS), 0.0, 0.0,
			0.0, 0.0, 12.0 / mass / (xS + yS), 0.0,
			0.0, 0.0, 0.0, 1.0);

		// this must be initialized, but can be reinitialized later if there is a need
		this->angularVelocity = Vec3(0, 0, 0);
		this->velocity = Vec3(0, 0, 0);
		//this->orientation = Quaternion<float>(1.0, 1.0, 1.0, 0.0);
		setOrientation(Quat(1.0, 1.0, 1.0, 0));
		//this->orientation = this->orientation0;
		this->angularMomentum = Vec3(0.0, 0.0, 0.0);
		this->angularVelocity = Vec3(0.0, 0.0, 0.0);
		this->torqueAccumulator = Vec3(0, 0, 0);
	}

	/**
	*converts from object space to world space according to:
	*http://www.codinglabs.net/article_world_view_projection_matrix.aspx
	*/
	XMMATRIX get_obj2World_matrix()
	{
		XMMATRIX rot = getRotationMatrix();
		XMMATRIX translation = XMMatrixTranslation(position.x, position.y, position.z);
		/**ntlMat4f scale(size.x, 0, 0, 0,
		0, size.y, 0, 0,
		0, 0, size.z, 0,
		0, 0, 0, 1);*/
		XMMATRIX scale = XMMatrixScaling(size.x, size.y, size.z);

		return scale*rot*translation;
	}

	XMMATRIX getRotationMatrix()
	{

		//XMFLOAT4 orientation(1.0, 1.0, 1.0, cos(0));
		//return XMMatrixRotationQuaternion(XMLoadFloat4(&orientation));
		return orientation.getRotMat().toDirectXMatrix();
	}

	void setOrientation(Quat ort)
	{
		//cout << "w:"<<to_string(ort.w) << endl;
		//XMVECTOR r = XMQuaternionRotationAxis(XMVectorSet(ort.x, ort.y, ort.z, 0.0f), ort.w);
		////XMVECTOR r = XMQuaternionRotationAxis(XMVectorSet(0,0,1, 0.0f), 0);
		//r = XMQuaternionNormalize(r);
		//XMFLOAT4 rr;
		//rr.x = XMVectorGetX(r);
		//rr.y = XMVectorGetY(r);
		//rr.z = XMVectorGetZ(r);
		//rr.w = XMVectorGetW(r);
		////XMFLOAT4 orientation(rr);
		//orientation0 = rr;
		//this->orientation = this->orientation0;
		orientation = ort.unit();
	}

};
#endif