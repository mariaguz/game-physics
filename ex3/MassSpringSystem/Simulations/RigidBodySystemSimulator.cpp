#include "RigidBodySystemSimulator.h"


RigidBodySystemSimulator::RigidBodySystemSimulator()
{
	notifyCaseChanged(m_iTestCase);	
}

const char * RigidBodySystemSimulator::getTestCasesStr()
{
	return "Demo1, Demo2, Demo3, Demo4";
}



const char * RigidBodySystemSimulator::getCurrentBoxCasesStr()
{
	
	return "Body0, Body1, Body2, Body3";
}

void RigidBodySystemSimulator::initUI(DrawingUtilitiesClass * DUC)
{
	this->DUC = DUC;
	TwType TW_TYPE_TESTCASE = TwDefineEnumFromString("Current Box", getCurrentBoxCasesStr());
	switch (m_iTestCase)
	{
	case 0:
		break;
	case 1:
		break;
	case 2:
		break;
	case 3:
		TwAddVarRW(this->DUC->g_pTweakBar, "Box to rotate", TW_TYPE_TESTCASE, &currInd, "");
		TwAddVarRW(this->DUC->g_pTweakBar, "Rotate", TW_TYPE_BOOLCPP, &rotator, "");
		break;
	default:break;
	}
}

void RigidBodySystemSimulator::reset()
{
	m_mouse.x = m_mouse.y = 0;
	m_trackmouse.x = m_trackmouse.y = 0;
	m_oldtrackmouse.x = m_oldtrackmouse.y = 0;
	rigidbodies.clear();
	currInd = 0;
}

void RigidBodySystemSimulator::drawFrame(ID3D11DeviceContext* pd3dImmediateContext)
{
	switch (m_iTestCase)
	{
	case 0:
		break;
	case 1:
		drawDemo2();
		break;
	case 2:
		drawDemo3();
		break;
	case 3:
		drawDemo4();
		break;
	}
}


void RigidBodySystemSimulator::onClick(int x, int y)
{
	m_trackmouse.x += x - m_oldtrackmouse.x;
	m_trackmouse.y += y - m_oldtrackmouse.y;
}

void RigidBodySystemSimulator::onMouse(int x, int y)
{
	m_oldtrackmouse.x = x;
	m_oldtrackmouse.y = y;
}

int RigidBodySystemSimulator::getNumberOfRigidBodies()
{
	return rigidbodies.size();
}

Vec3 RigidBodySystemSimulator::getPositionOfRigidBody(int i)
{
	return rigidbodies[i].position;
}


Vec3 RigidBodySystemSimulator::getLinearVelocityOfRigidBody(int i)
{
	return rigidbodies[i].velocity;
}



Vec3 RigidBodySystemSimulator::getAngularVelocityOfRigidBody(int i)
{
	return rigidbodies[i].angularVelocity;
}


void RigidBodySystemSimulator::addRigidBody(Vec3 position, Vec3 size, int mass)
{
	RigidBodySystem body(position, size, mass);
	rigidbodies.push_back(body);
}

void RigidBodySystemSimulator::setOrientationOf(int i, Quat orientation)
{
	rigidbodies[i].setOrientation(orientation);
	resetInertiaTensor(i);
}


void RigidBodySystemSimulator::setVelocityOf(int i, Vec3 velocity)
{
	rigidbodies[i].velocity = velocity;
}

void RigidBodySystemSimulator::resetInertiaTensor(int i) {
	RigidBodySystem body = rigidbodies[i];
	Mat4 rot = body.getRotationMatrix();
	Mat4 rotT = rot;
	rotT.transpose();
	body.inertiaTensorInverse = (rot*body.inertiaTensorInverse0)*rotT;
}

void RigidBodySystemSimulator::applyForceOnBody(int i, Vec3 loc, Vec3 force)
{
	RigidBodySystem &body = rigidbodies[i];
	body.forceAccumulator += force;
	Vec3 pos = getPositionOfRigidBody(i);
	Vec3 nLoc = loc - pos;
	body.torqueAccumulator += cross(nLoc, force);
	cout << "body " << to_string(i) << " torque:" << body.torqueAccumulator << endl;
}

void RigidBodySystemSimulator::externalForcesCalculations(float timeElapsed)
{
}

void RigidBodySystemSimulator::simulateTimestep(float timeStep)
{
	if (rotator)
	{
		int ax = rand() % 3;
		int sig = rand() % 2;
		RigidBodySystem &body = rigidbodies[currInd];

		Vec3 rot(0, 0, 0);
		rot[ax] = 3;
		if (sig == 1)
		{
			rot *= -1;
		}
		body.angularMomentum += rot;
		cout << "rotating:" << rotator << "<" << currInd << endl;
		rotator = false;
	}
	switch (m_iTestCase)
	{
	case 0:
		perform_time_step(DEMO1_TIMESTEP);
		break;
	case 1:
		perform_time_step(DEMO2_TIMESTEP);
		break;
	case 2:
		perform_time_step(timeStep);
		break;
	case 3:
		perform_time_step(timeStep);
	default:
		break;
	}
}

void RigidBodySystemSimulator::perform_time_step(float timestep)
{
	string str;
	for (int i = 0; i < getNumberOfRigidBodies(); i++) {
		for (int j = i + 1; j < getNumberOfRigidBodies(); j++) {
			RigidBodySystem& a = rigidbodies[i];
			RigidBodySystem& b = rigidbodies[j];
			CollisionInfo cInfo = checkCollisionSAT(Mat4(a.get_obj2World_matrix()), Mat4(b.get_obj2World_matrix()));
			if (cInfo.isValid) {
				Vec3 x_a = cInfo.collisionPointWorld - a.position;
				Vec3 x_b = cInfo.collisionPointWorld - b.position;
				Vec3& n = cInfo.normalWorld;
				Vec3 vel_relative = a.velocity + cross(a.angularVelocity, x_a) - b.velocity - cross(b.angularVelocity, x_b);
				if (dot((vel_relative), cInfo.normalWorld) > 0.0f) continue;
				double impulse = -1 * (1.0 + ELASTICITY)*dot((vel_relative), cInfo.normalWorld);
				impulse /= (1.0 / a.mass + 1.0 / b.mass + dot(cross(a.inertiaTensorInverse.transformVector(cross(x_a, n)), x_a) +
					cross(b.inertiaTensorInverse.transformVector(cross(x_b, n)), x_b)
					, n));
				a.colVelocity += n*impulse / a.mass;
				b.colVelocity -= n*impulse / b.mass;
				a.colAngularMomentum += cross(x_a, n*impulse);
				b.colAngularMomentum -= cross(x_b, n*impulse);
			}
		}
	}
	for (int i = 0; i < getNumberOfRigidBodies(); i++) {
		RigidBodySystem &b = rigidbodies[i];

		b.position += b.velocity * timestep;
		b.velocity += b.forceAccumulator / b.mass * timestep + b.colVelocity;

		Vec3& w = b.angularVelocity;
		GamePhysics::Quat quat(w.x, w.y, w.z, 0.0);
		b.orientation += timestep / 2.0f * quat*b.orientation;
		b.orientation = b.orientation.unit();

		
		b.angularMomentum += b.torqueAccumulator*timestep + b.colAngularMomentum;
		Mat4 rot = b.getRotationMatrix();
		Mat4 rotT = rot;
		rotT.transpose();
		b.inertiaTensorInverse = (rot*b.inertiaTensorInverse0)*rotT;


		Vec3& L = b.angularMomentum;
		b.angularVelocity = b.inertiaTensorInverse0.transformVector(L);
		//This part is made according to the slides and gives worse results:
		/**b.angularVelocity = b.inertiaTensorInverse.transformVector(L);*/
		
		b.forceAccumulator = 0;
		b.torqueAccumulator = 0;
		b.colVelocity = 0;
		b.colAngularMomentum = 0;
	}
}

void RigidBodySystemSimulator::notifyCaseChanged(int testCase)
{
	m_iTestCase = testCase;
	int ind;
	rotator = false;
	currInd = 0;
	printed = false;
	printed2 = false;
	stringstream ss4;
	string str;
	switch (m_iTestCase)
	{
	case 0:
		cout << "demo1 !\n";
		cout << "in notif case 0!" << endl;
		reset();
		addRigidBody(Vec3(0.0, 0.0, 0.0), Vec3(1.0, 0.6, 0.5), 2);
		ind = 0;
		setVelocityOf(ind, Vec3(0.0, 0.0, 0.0));
		setOrientationOf(ind, Quat(Vec3(0.0, 0.0, 1.0), (float)M_PI_2));
		applyForceOnBody(ind, Vec3(0.3, 0.5, .25), Vec3(1, 1, 0));
		simulateTimestep(DEMO1_TIMESTEP);
		drawDemo1();
		break;
	case 1:
		cout << "demo2 !\n";
		reset();
		addRigidBody(Vec3(0.0, 0.0, 0.0), Vec3(1.0, 0.6, 0.2), 2);
		ind = 0;
		setVelocityOf(ind, Vec3(0.0, 0.0, 0.0));
		setOrientationOf(ind, Quat(Vec3(0.0, 0.0, 1.0), (float)M_PI_2));
		applyForceOnBody(ind, Vec3(0.3, 0.5, 0.25), Vec3(10, 1, 0));
		break;
	case 2:
		cout << "demo3 !\n";
		reset();
		addRigidBody(Vec3(0.0, 0.0, 0.0), Vec3(1.0, 0.6, 0.5), 2);
		addRigidBody(Vec3(-2.0, 0.0, 0.0), Vec3(1.0, 0.6, 0.5), 2);
		setVelocityOf(0, Vec3(-0.5, 0.0, 0.0));
		setVelocityOf(1, Vec3(0.3, 0.0, 0.0));
		setOrientationOf(0, Quat(Vec3(0.0, 0.0, 1.0), ((float)M_PI_4)));
		setOrientationOf(1, Quat(Vec3(1.0, 1.0, 1.0), 0));
		break;
	case 3:
		reset();
		addRigidBody(Vec3(-2.0, 2.0, 2.0), Vec3(0.8, 0.6, 0.5), 2);
		addRigidBody(Vec3(2.0, 2.0, 2.0), Vec3(1.0, 0.6, 0.5), 2);
		addRigidBody(Vec3(-2.0, -2.0, -2.0), Vec3(0.25, 0.6, 0.5), 2);
		addRigidBody(Vec3(2.0, -2.0, -2.0), Vec3(1.0, 1.0, 0.5), 2);
		setVelocityOf(0, Vec3(0.0, 0.0, 0.0) - Vec3(-2.0, 2.0, 2.0));
		setVelocityOf(1, Vec3(0.0, 0.0, 0.0) - Vec3(2.0, 2.0, 2.0));
		setVelocityOf(2, Vec3(0.0, 0.0, 0.0) - Vec3(-2.0, -2.0, -2.0));
		setVelocityOf(3, Vec3(0.0, 0.0, 0.0) - Vec3(2.0, -2.0, -2.0));
		setOrientationOf(0, Quat(Vec3(0.0, 0.0, 1.0), (float)M_PI_4));
		setOrientationOf(1, Quat(Vec3(1.0, 1.0, 1.0), 0));
		setOrientationOf(2, Quat(Vec3(0.0, 0.0, 1.0), (float)M_PI_4));
		setOrientationOf(3, Quat(Vec3(1.0, 1.0, 1.0), 0));
	default:
		break;
	}
}

void RigidBodySystemSimulator::drawDemo1()
{
	if (!printed)
	{
		cout << "linear velocity:" << rigidbodies[0].velocity << endl;
		cout << "angular velocity:" << rigidbodies[0].angularVelocity << endl;
		Vec3 xa_world = Vec3(-0.3f, -0.5f, -0.25f) - rigidbodies[0].position;
		Vec3 velocityA = rigidbodies[0].velocity + cross(rigidbodies[0].angularVelocity, xa_world);
		cout << "worldspace velocity:" << velocityA << endl;
		printed = true;
	}
}

void RigidBodySystemSimulator::drawDemo2()
{
	drawAllBoxes();
}

void RigidBodySystemSimulator::drawDemo3()
{
	drawAllBoxes();
}

void RigidBodySystemSimulator::drawDemo4()
{
	drawAllBoxes();
}

void RigidBodySystemSimulator::drawBox(RigidBodySystem body)
{
	std::mt19937 eng;
	std::uniform_real_distribution<float> randCol(0.0f, 1.0f);
	std::uniform_real_distribution<float> randPos(-0.5f, 0.5f);
	DUC->setUpLighting(Vec3(), 0.4*Vec3(1, 1, 1), 100, 0.6*Vec3(randCol(eng), randCol(eng), randCol(eng)));

	XMMATRIX obj2WorldMat = body.get_obj2World_matrix();
	DUC->drawRigidBody(obj2WorldMat);
}

void RigidBodySystemSimulator::drawAllBoxes()
{
	for (vector<RigidBodySystem>::iterator it = rigidbodies.begin(); it != rigidbodies.end(); ++it)
	{
		drawBox(*it);
	}
}

void RigidBodySystemSimulator::single_print(string str)
{
	if (!printed2)
	{
		cout << str << endl;
	}

}