#ifndef RIGIDB_H
#define RIGIDB_H
#include "Simulator.h"
#include <DirectXMath.h>
typedef XMVECTOR QVec;
typedef XMMATRIX Matr4;
class RigidBody
{
public:
	float massInverse;
	Vec3 position;
	Vec3 velocity;
	Vec3 size;
	Matr4 inertiaTensorInverse;
	Quat orientation;
	Vec3 angularVelocity;
	Vec3 forceAccumulator;
	Vec3 torqueAccumulator;
	Matr4 transform;

	RigidBody(Vec3 position, Vec3 size, int mass) : position(position), 
													size(size), massInverse(1/float(mass))
													{}

};
#endif